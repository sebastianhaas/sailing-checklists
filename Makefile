compile:  build/checklists.pdf build/safety-instructions.adoc

build/checklists.pdf:
		bundle exec asciidoctor-pdf \
		-r ./asciidoctor-extensions-lab/lib/git-metadata-preprocessor.rb \
		-a pdf-theme=resources/themes/sailing-checklist-theme.yml \
		-a media=print \
		src/checklists.adoc \
		--trace \
		-o build/checklists.pdf

build/safety-instructions.adoc:
		bundle exec asciidoctor-pdf \
		-r ./asciidoctor-extensions-lab/lib/git-metadata-preprocessor.rb \
		-a media=print \
		src/safety-instructions.adoc \
		-o build/safety-instructions.pdf

clean:
		rm build/*.pdf
